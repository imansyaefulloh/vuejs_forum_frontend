import Vue from '../main'

const store = {}

export default store

store.getSections = () => {
    return new Promise((resolve, reject) => {
        Vue.http({ url: 'sections', method: 'GET' }).then(response => {
            resolve(response.data.data);
        })
    });
}

store.getTopicsBySection = (id) => {
    return new Promise((resolve, reject) => {
        Vue.http({ url: 'topics', method: 'GET', params: { 'section_id': id } }).then(response => {
            resolve(response.data.data);
        });
    });
}

store.getTopicById = (id) => {
    return new Promise((resolve, reject) => {
        Vue.http({
            url: 'topics/' + id,
            method: 'GET'
        }).then(response => {
            resolve(response.data.data);
        });
    });
}


store.replyToTopicById = (id, body) => {
    return new Promise((resolve, reject) => {
        Vue.http.post('topics/' + id + '/posts', {body: body}).then(response => {
            resolve(response.data.data);
        })
    })
}


store.createTopic = (section, title, body) => {
    return new Promise((resolve, reject) => {
        Vue.http.post('topics', {title: title, body: body, section_id: section}).then(response => {
            resolve(response.data.data);
        }, response => {
            reject(response.data);
        })
    })
}














