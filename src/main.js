import Vue from 'vue'
import App from './components/App'
import Home from './components/Home'
import Topics from './components/Topics'
import Topic from './components/Topic'
import NewTopic from './components/NewTopic'
import Signin from './components/Signin'
import Signup from './components/Signup'

import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

Vue.use(VueRouter)
Vue.use(VueResource)

Vue.http.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('id_token');
Vue.http.options.root = 'http://l52-forum-api.app:8000';

export var router = new VueRouter
export default Vue

router.map({
    '/': {
        name: 'home',
        component: Home
    },
    '/signin': {
        name: 'auth.signin',
        component: Signin
    },
    '/signup': {
        name: 'auth.signup',
        component: Signup
    },
    '/sections/:sectionId': {
        name: 'sections',
        component: Topics
    },
    '/topics/:topicId': {
        name: 'topics',
        component: Topic
    },
    '/topics/new': {
        name: 'topic.new',
        component: NewTopic
    }
});

router.start(App, '#app');





















