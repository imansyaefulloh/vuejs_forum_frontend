import Vue from '../main'

import {router} from '../main'


export default {
    user: {
        authenticated: false,
        profile: null
    },
    check() {
        if (localStorage.getItem('id_token') !== null) {
            Vue.http({
                url: 'users',
                method: 'GET'
            }).then(response => {
                this.user.authenticated = true;
                this.user.profile = response.data.data;
            });
        }
    },
    signin(context, email, password) {
        Vue.http.post('auth/signin', {email: email, password: password}).then(response => {
            
            context.success = true;

            localStorage.setItem('id_token', response.data.meta.token);

            Vue.http.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('id_token');

            this.user.authenticated = true;
            this.user.profile = response.data.data;

        }, response => {
            context.response = response.data;
            context.error = true;
        })
    },
    signup(context, email, username, password) {
        Vue.http.post('auth/signup', {email: email, username: username, password: password}).then(response => {
            context.success = true
        }, response => {
            context.response = response.data
            context.error = true
        })
    },
    signout() {
        localStorage.removeItem('id_token');
        this.user.authenticated = false;
        this.user.profile = null;

        router.go({
            'name': 'home'
        });
    }
}